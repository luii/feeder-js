// Module dependencies
var debug = require('debug')('feeder:app')
var feed = require('./feed').feed
var fs = require('fs')

/**
 * Feeder app constructor
 * @property {Map} feeds Holds all the feed instances in there
 */
function Feeder() {
  this.feeds = new Map()
}


/**
 * Adds another feed instance to the app
 * @param {Function} feed Feed instance
 */
Feeder.prototype.add = function (feed) {
  if (this.feeds.has(feed.name)) {
    return this.feeds.get(feed.name)
  }

  this.feeds.set(feed.name, feed)
}


/**
 * Wachtes a specified feed
 * @param  {String} name Name to identify the feed
 */
Feeder.prototype.watch = function (name) {
  var feed
  if (this.feeds.has(name)) {
    feed = this.feeds.get(name)

    feed.watch()
  }
}


/**
 * Watches all feeds
 */
Feeder.prototype.watchAll = function () {
  this.feeds.forEach(function (feed) {
    feed.watch()
  })
}


/**
 * Stops the feed being watched
 * @param  {String} name Name to identify the feed
 * @return {void}
 */
Feeder.prototype.stop = function (name) {
  var feed
  if (this.feeds.has(name)) {
    feed = this.feeds.get(name)
    feed.stop()
  }
}


/**
 * Stops all feeds from being watched
 * @return {void}
 */
Feeder.prototype.stopAll = function () {
  this.feeds.forEach(function (feed) {
    feed.stop()
  })
}


// exports public interface
exports.Feeder = Feeder
