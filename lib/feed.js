var EventEmitter = require('events').EventEmitter
var debug = require('debug')('feeder:feed')
var inherit = require('util').inherits

/**
 * Feed constructor
 * @param {String} name       Name of the feed
 * @param {String} link       Link of the feed
 * @param {Number} [interval] Interval for long polling
 *                            defaults to 30000ms
 */
function Feed (name, link, interval) {
  debug('called Feed constructor')

  EventEmitter.call(this)

  this.id = null
  this.name = name
  this.link = link
  this.watching = false
  this.interval = 30000
}


// inherit Eventemitter prototrype
inherit(Feed, EventEmitter)


/**
 * Abstract request function that every Feed must have
 * @access protected
 * @param  {Function} cb Callback function
 * @return {Function}    Returns and invokes the callback
 */
Feed.prototype._request = function(cb) {
  return cb(new Error('Please overwrite this method'))
}


Feed.prototype.watch = function(interval, cb) {
  if (! interval) {
    cb = interval
    interval = null
  } else {
    this.interval = interval
  }

  this.id = setInterval(this._request, this.interval, cb)
  this.watching = true
  return
}


/**
 * Clears the interval for this feed and stops watching
 * @access public
 * @return void
 */
Feed.prototype.stop = function() {
  if (this.watching) {
    debug('clearInterval on feed')

    clearInterval(this.id)
    this.id = null
    this.watching = false
  }
  return
}


// exports public interface
exports.Feed = Feed
exports.feed = feed
